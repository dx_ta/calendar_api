require 'rubygems'
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'calendar_api/version'

spec = Gem::Specification.new do |s|
  s.name = "calendar_api"
  s.version = CalendarAPI::VERSION
  s.summary = "Calendar API by dxta"
  s.description = "Ruby library to access Calendar service"
  s.author = ["dxta"]
  s.email = ["minh.ta@aalto.fi"]
  s.license = ""
  s.homepage = "http://dxta.github.io/"

  s.files = `git ls-files`.split($\)
  s.test_files = Dir.glob("rspec/*_spec.rb")
  s.require_paths = ["lib"]
  s.has_rdoc = true
  s.required_ruby_version = '>= 1.9.3'

  #s.add_runtime_dependency
  #s.add_development_dependency
  s.add_dependency("activesupport")
  s.add_development_dependency 'bundler'
  s.add_development_dependency 'rake'
  s.add_development_dependency 'rspec'
  s.add_development_dependency 'pry'
end

