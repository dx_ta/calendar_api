require "json"
require "net/http"

begin
  require 'active_support'
rescue LoadError
  require 'rubygems'
  require 'active_support'
end

require "calendar_api/version"
require "calendar_api/config"
require 'calendar_api/url_helpers'
require 'calendar_api/response'

require "calendar_api/api"
require "calendar_api/api/event"


module CalendarAPI
end
