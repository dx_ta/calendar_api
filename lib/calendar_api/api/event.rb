module CalendarAPI
  module API
    # User API
    #
    # Handling all Event requests
    class Event < CalendarAPI::API::Core

      def self.root_endpoint
        "#{CalendarAPI.api_uri}/v1/events"
      end

      # Create a event API
      #
      # @param event [Hash] event data
      # @param data_type [String] the return type, `json` or `xml`
      #
      # @return [Response] return as Response object.
      #
      # @example
      #     CalendarAPI::API::Event.create({email: "",phone: ""})
      def self.create event,data_type="json"
        uri = "#{root_endpoint}"
        params = {:event => event}
        raw_resp = post_request(uri,params)
        resp = Response.new(raw_resp)
      end

      # Index events
      #
      # @param data_type [String] the return type, `json` or `xml`
      #
      # @return [Response] return as Response object.
      #
      # @example
      #     CalendarAPI::API::Event.index({detail: true})
      def self.index options={},data_type="json"
        uri = "#{root_endpoint}"
        params = {:options => options}
        raw_resp = get_request(uri,params)
        resp = Response.new(raw_resp)
      end

      # Get Event API
      #
      # @param event [Hash] event data
      # @param data_type [String] the return type, `json` or `xml`
      #
      # @return [Response] return as Response object.
      #
      # @example
      #     CalendarAPI::API::Event.get(1,{detail: true })
      def self.get event_id,options={},data_type="json"
        uri = "#{root_endpoint}/#{event_id}"
        params = {:options => options}
        raw_resp = get_request(uri,params)
        resp = Response.new(raw_resp)
      end

      # Update Event API
      #
      # @param event [Hash] event data
      # @param data_type [String] the return type, `json` or `xml`
      #
      # @return [Response] return as Response object.
      #
      # @example
      #     CalendarAPI::API::Event.update(1,{name: ""})
      def self.update event_id,event,data_type="json"
        uri = "#{root_endpoint}/#{event_id}"
        params = {:event => event}
        raw_resp = put_request(uri,params)
        resp = Response.new(raw_resp)
      end

      # Delete Event API
      #
      # @param event [Hash] event data
      # @param data_type [String] the return type, `json` or `xml`
      #
      # @return [Response] return as Response object.
      #
      # @example
      #     CalendarAPI::API::Event.remove(1)
      def self.remove event_id,options={},datatype="json"
        uri = "#{root_endpoint}/#{event_id}"
        params = {:options => options}
        raw_resp = delete_request(uri,params)
        resp = Response.new(raw_resp)
      end

    end
  end
end

