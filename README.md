# CalendarAPI

Ruby connector to access the Calendar API

## Installation
    put this linve to Gemfile
    gem 'calendar_api',:git => "https://ductm310@bitbucket.org/ductm310/calendar_api.git"

## Usage

    require 'ckfapi'

    Ckfapi.api_key = 'api-key'
    Ckfapi.api_uri = 'http://your-api-uri'

    2.1.1 (main):0 > CalendarAPI::API::Event.index()
    => {"events"=>[]}
    2.1.1 (main):0 > st = Time.now + 600
    => 2015-10-05 14:42:11 +0300
    2.1.1 (main):0 > et = st + 1000
    => 2015-10-05 14:58:51 +0300
    2.1.1 (main):0 > CalendarAPI::API::Event.create({title: "first event", location: "Otaniemi", start_time: st, end_time: et, note: "testing"})
    => {"event"=>
      {"_id"=>{"$oid"=>"56125fe2a0e7b20269000000"},
      "end_time"=>"2015-10-05T11:58:51.000+00:00",
      "location"=>"Otaniemi",
      "note"=>"testing",
      "parent_event_id"=>nil,
      "repeat"=>-1,
      "repeat_end_at"=>nil,
      "start_time"=>"2015-10-05T11:42:11.000+00:00",
      "title"=>"first event"}}
    2.1.1 (main):0 > CalendarAPI::API::Event.index()
    => {"events"=>
      [{"_id"=>{"$oid"=>"56125fe2a0e7b20269000000"},
        "end_time"=>"2015-10-05T11:58:51.000+00:00",
        "location"=>"Otaniemi",
        "note"=>"testing",
        "parent_event_id"=>nil,
        "repeat"=>-1,
        "repeat_end_at"=>nil,
        "start_time"=>"2015-10-05T11:42:11.000+00:00",
        "title"=>"first event"}]}
    2.1.1 (main):0 > CalendarAPI::API::Event.get("56125fe2a0e7b20269000000")
    => {"event"=>
      {"_id"=>{"$oid"=>"56125fe2a0e7b20269000000"},
      "end_time"=>"2015-10-05T11:58:51.000+00:00",
      "location"=>"Otaniemi",
      "note"=>"testing",
      "parent_event_id"=>nil,
      "repeat"=>-1,
      "repeat_end_at"=>nil,
      "start_time"=>"2015-10-05T11:42:11.000+00:00",
      "title"=>"first event"}}
    2.1.1 (main):0 >

### Using Ruby on Rails
Put this config in ```config/initializers/calendar_api.rb. ```

Copyright
==========================

Copyright (c) 2015 dxta.
